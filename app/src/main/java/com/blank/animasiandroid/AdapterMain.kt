package com.blank.animasiandroid

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_view.view.*

class AdapterMain(private val data: MutableList<Model>) :
    RecyclerView.Adapter<AdapterMain.ViewHolder>() {

    var listener: ((View, View, Model) -> Unit)? = null
        set(value) {
            field = value
        }


    class ViewHolder(private val v: View) : RecyclerView.ViewHolder(v) {
        companion object {
            fun created(parent: ViewGroup): ViewHolder = ViewHolder(
                LayoutInflater.from(parent.context).inflate(R.layout.item_view, parent, false)
            )
        }

        fun bind(model: Model) {
            v.ivFoto.setImageResource(model.img)
            v.tvTitle.text = model.nama
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder =
        ViewHolder.created(parent)

    override fun getItemCount(): Int = data.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val model = data[position]
        holder.bind(model)
        holder.itemView.setOnClickListener {
            listener?.invoke(holder.itemView.ivFoto, holder.itemView.tvTitle, model)
        }
    }
}