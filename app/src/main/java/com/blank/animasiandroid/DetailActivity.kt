package com.blank.animasiandroid

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_detail.*

class DetailActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)

        val model = intent.extras?.getParcelable<Model>("data")
        tvTitle.text = model?.nama
        model?.img?.let { imageView.setImageResource(it) }
    }
}