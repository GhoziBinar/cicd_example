package com.blank.animasiandroid

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityOptionsCompat
import androidx.core.util.Pair
import androidx.core.util.Pair.create
import androidx.core.view.ViewCompat
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val adapter = AdapterMain(dummy())
        rvMain.layoutManager = LinearLayoutManager(this)
        rvMain.adapter = adapter

        adapter.listener = { image, title, model ->
            val imageViewTransitionPair: Pair<View, String> =
                create(
                    image,
                    ViewCompat.getTransitionName(image)
                )

            val titleTransitionPair: Pair<View, String> =
                create(
                    title,
                    ViewCompat.getTransitionName(title)
                )

            val options = ActivityOptionsCompat.makeSceneTransitionAnimation(
                this,
                imageViewTransitionPair,
                titleTransitionPair
            )

            startActivity(
                Intent(this, DetailActivity::class.java).also {
                    it.putExtra("data", model)
                },
                options.toBundle()
            )
        }
    }
}

fun dummy(): MutableList<Model> = mutableListOf(
    Model("Ghozi", R.drawable.metal),
    Model("Aris", R.drawable.metal),
    Model("Wita", R.drawable.metal),
    Model("Ariq", R.drawable.hutam),
    Model("Bayu", R.drawable.metal),
    Model("Nug", R.drawable.metal)
)