package com.blank.animasiandroid

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
class Model(val nama: String, val img: Int) : Parcelable